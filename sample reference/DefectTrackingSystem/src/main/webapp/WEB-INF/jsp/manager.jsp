<!DOCTYPE html>
<html lang="en">
<head>
  <title>Defect Tracking System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 800px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:  #FFE4E1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
    * {box-sizing: border-box}
body {font-family: "Lato", sans-serif; background-color: #FFF5EE;}

/* Style the tab */
.tab {
  float: left;
  border: 1px ;
  
  width: 100%;
  height: 300px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: #4b0082;
  padding: 17px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 16px;
}
.tab1 button {
  display: block;
  background-color: inherit;
  color: #4b0082;
  padding: 17px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 30px;
}
.tab a {
  display: block;
  background-color: inherit;
  color: #8a2be2;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 16px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: lightgray;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
   /*border: 1px solid #ccc; */ 
  width: 70%;
  border-left: none;
  height: 300px;
}

  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <br>
      <ul class="nav nav-pills nav-stacked">
        <div class="tab1">
          <li><button class="tablinks" onclick="openCity(event, 'manager')">Project Manager</button></li>
        </div>
      </ul>
      <br>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">       
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
      <br><br>
      <ul class="nav nav-pills nav-stacked">
         <div class="tab">
      <li><button class="tablinks" onclick="openCity(event, 'Summary')" id="defaultOpen">DashBoard</button></li>
      <li><button class="tablinks" onclick="openCity(event, 'Add New User')">Add New User</button></li>
      <li><button class="tablinks" onclick="openCity(event, 'Update user Details')">Update user Details</button></li>
      <li><button class="tablinks" onclick="openCity(event, 'Create Project')">Create Project</button></li>
      <li><button class="tablinks" onclick="openCity(event, 'View Defect Report')">View Defect Report</button></li>
      <li><button class="tablinks" onclick="openCity(event, 'View Project Members')">View Project Members</button></li>
      <li><button class="tablinks" onclick="openCity(event, 'User Details')">User Details</button></li>
      <li><a href="login.html">Logout</a></li>

       <br>
       
     </ul>
      <br><br>
      </div>
  
    <div class="col-sm-9">
      <h4><small>Project Manager Page</small></h4>
      <hr>
      <div id="Add New User" class="tabcontent">
        <!----------------------------------------------------------- ADD NEW USER ------------------------------------------------------------------->
      <h2>Add New User</h2><br>     
      <div class="container">
  <form class="form-horizontal" action="/action_page.php">

  <div class="form-group">
      <label class="control-label col-sm-2" for="name">Employee Name:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="name" placeholder="Enter Employee Name" name="name">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="id">Employee Id:</label>
      <div class="col-sm-4">
        <input type="number" class="form-control" id="id" placeholder="Enter Employee Id" name="id">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="role">Role:</label>
      <div class="col-sm-4">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Role    
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">Developer</a></li>
      <li><a href="#">Tester</a></li>
    </ul>
  </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="loginid">Login Id:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="loginid" placeholder="Enter Login Id" name="loginid">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-4">          
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="gender">Gender:</label>
      <div class="col-sm-4">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Gender    
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">Male</a></li>
      <li><a href="#">Female</a></li>
    </ul>
  </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-4">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="no">Contact Number:</label>
      <div class="col-sm-4">
      <input type="text" class="form-control" id="no" placeholder="Enter Contact Number" name="no" pattern="[5-9]{1}[0-9]{9}">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="address">Address:</label>
      <div class="col-sm-4">
       <textarea class="form-control" rows="5" id="address" placeholder="Enter Address" name="address"></textarea>
      </div>
   </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-success">Add</button>
      </div>
    </div>
  </form>
</div>    
      <br><br>
      </div>

<!------------------------------------------------------------------- UPDATE USER DETAILS ----------------------------------------------------------->

      <div id="Update user Details" class="tabcontent">
      <h2>Update user Details</h2><br>
      
      <div class="container">
 
  <form class="form-horizontal" action="/action_page.php">

  <div class="form-group">
      <label class="control-label col-sm-2" for="name">Employee Name:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="name" placeholder="Enter Employee Name" name="name">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="id">Employee Id:</label>
      <div class="col-sm-4">
        <input type="number" class="form-control" id="id" placeholder="Enter Employee Id" name="id">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="role">Role:</label>
      <div class="col-sm-4">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Role    
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">Developer</a></li>
      <li><a href="#">Tester</a></li>
    </ul>
  </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="loginid">Login Id:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="loginid" placeholder="Enter Login Id" name="loginid">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-4">          
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="gender">Gender:</label>
      <div class="col-sm-4">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Gender    
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">Male</a></li>
      <li><a href="#">Female</a></li>
    </ul>
  </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-4">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="no">Contact Number:</label>
      <div class="col-sm-4">
      <input type="text" class="form-control" id="no" placeholder="Enter Contact Number" name="no" pattern="[5-9]{1}[0-9]{9}">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="address">Address:</label>
      <div class="col-sm-4">
        <textarea class="form-control" rows="5" id="address" placeholder="Enter Address" name="address"></textarea>
      </div>
   </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-success">Update</button>
      </div>
    </div>
  </form>
</div>    
      <br><br>
      </div>

<!------------------------------------------------------------------- CREATE PROJECT----------------------------------------------------------->

<div id="Create Project" class="tabcontent">
      <h2>Create Project</h2><br>
      
      <div class="container">
 
  <form class="form-horizontal" action="/action_page.php">
    <div class="form-group">
      <label class="control-label col-sm-2" for="role">Project Assigned To:</label>
      <div class="col-sm-4">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">User Role   
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">Developer</a></li>
      <li><a href="#">Tester</a></li>
    </ul>
  </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="id">Employee Id:</label>
      <div class="col-sm-4">
        <input type="number" class="form-control" id="id" placeholder="Enter Employee Id" name="id">
      </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="id">Project Id:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="pid" placeholder="Enter Project Id" name="pid">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="projectname">Project Name:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="projectname" placeholder="Enter Project Name" name="projectname">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="projectdesc">Project Description:</label>
      <div class="col-sm-4">
        <textarea class="form-control" rows="5" id="projectdesc" placeholder="Type Project Description" name="projectdesc"></textarea>
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="sdate">Submission Date:</label>
      <div class="col-sm-4">
        <input type="date" class="form-control" id="sdate" name="sdate">
      </div>
   </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="duration">Project Duration:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="duration" placeholder="Enter The Duration of Project" name="duration">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="no">Contact Number:</label>
      <div class="col-sm-4">
      <input type="text" class="form-control" id="no" placeholder="Enter Contact Number" name="no" pattern="[5-9]{1}[0-9]{9}">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-4">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="duration">Project Lead Name:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="leadname" placeholder="Enter The Project Lead Name" name="leadname">
      </div>
   </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-success">Create</button>
      </div>
    </div>
  </form>
</div>    
      <br><br>
      </div>

 <!---------------------------------------------------------VIEW DEFECT REPORT------------------------------------------------------->
 <div id="View Defect Report" class="tabcontent">
      <h2>View Defect Report</h2><br>
      <div class="container"> 
        <table class="table table-striped">
    <thead>
      <tr>
        <th>Defect Id</th>
        <th>Defect Name</th>
        <th>Priority</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
</div>    
      <br><br>
      </div>

<!---------------------------------------------------------VIEW PROJECT MEMBERS------------------------------------------------------->

<div id="View Project Members" class="tabcontent">
      <h2>View Project Members</h2><br>
      <div class="container"> 
      <!------  <form class="form-horizontal" action="/action_page.php">
    <div class="form-group">
      <label class="control-label col-sm-2" for="role">List of Projects:</label>
      <div class="col-sm-10">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Project List  
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">PID001</a></li>
     <li><a href="#">PID002</a></li>
     <li><a href="#">PID003</a></li>         /----------- sample project names ---------------------------/
     <li><a href="#">PID004</a></li>
     <li><a href="#">PID005</a></li>
    </ul>
  </div>
   </div>
</form> --------------->
<table class="table table-striped">
    <thead>
      <tr>
        <th>Project Id</th>
        <th>Project Name</th>
        <th>Team Lead Name</th>
        <th>Developers Name</th>
        <th>Testers Name</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td><td></td>
      </tr>
      <tr><td></td><td></td><td></td><td></td><td></td></tr>
       <tr><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td></td><td></td><td></td><td></td></tr>
         <tr><td></td><td></td><td></td><td></td><td></td></tr>
          <tr><td></td><td></td><td></td><td></td><td></td></tr>
           <tr><td></td><td></td><td></td><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td><td></td></tr>
             <tr><td></td><td></td><td></td><td></td><td></td></tr>
              <tr><td></td><td></td><td></td><td></td><td></td></tr>
               <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                 <tr><td></td><td></td><td></td><td></td><td></td></tr>
                  <tr><td></td><td></td><td></td><td></td><td></td></tr>
                   <tr><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr><td></td><td></td><td></td><td></td><td></td></tr>
      </tbody>
  </table>
</div>
      <br><br>
      </div>

<!-------------------------------------------------------------------SUMMARY------------------------------------------------------------------------->

<div id="Summary" class="tabcontent">
      <h2>Summary of Project</h2><br>
      <div class="container"> 
        <table class="table table-striped">
    <thead>
      <tr>
        <th>Project Id</th>
        <th>Project Name</th>
        <th>Team Lead Name</th>
        <th>Works Done (%)</th>
        <th>Works To Be Done (%)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td><td></td>
      </tr>
     
      
    </tbody>
  </table>
</div>
</div>
<!-------------------------------------------------------------USER DETAILS----------------------------------------------------------------->    
<div id="User Details" class="tabcontent">
      <h2>User Details</h2><br>
      <div class="container"> 
        <table class="table table-striped">
    <thead>
      <tr>
        <th>Employee Name</th>
        <th>Employee Id</th>
        <th>Role</th>
        <th>Login Id</th>
        <th>Password</th>
        <th>Email</th>
        <th>Contact Number</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>

      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      </tbody>
  </table>
</div>
</div>
  <!---------------------------------------------------------------MANAGER--------------------------------------------------------------------->
  <div id="manager" class="tabcontent">
    <h2 style="text-align:center;">Profile</h2><br>
    
    <div class="container">
  
  <form class="form-horizontal" action="/action_page.php">
  <div class="form-group">
    <label class="control-label col-sm-3" for="name">Name:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="managername" placeholder="Name" name="managername">
  </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="emailid">Email ID:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="emailid" placeholder="Email_ID" name="emailid">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="Changepassword">Change Password:</label>
    <div class="col-sm-4">
      <input type="password" placeholder="*************" class="form-control" required id="changepassword" name="changepassword">
    </div>                                    
  
    <br></br>
  
  <div class="form-group">        
    <div class="col-sm-offset-4 col-sm-2">
      <button type="submit" class="btn btn-success">Update</button>
    </div>
  </div>
  </form>
  </div>    
    </div>
      <br><br>
      </div>
</div>    

<!-----------------------------------------------------------------SCRIPT---------------------------------------------------------------------->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<footer class="container-fluid">
  <p><center>� Cognizant</center></p>
</footer>

</body>
</html>