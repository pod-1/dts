<!DOCTYPE html>
<html lang="en">
<head>
  <title>Developer</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 800px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:  #FFE4E1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
    * {box-sizing: border-box}
body {font-family: "Lato", sans-serif; background-color: #FFF5EE;}

/* Style the tab */
.tab {
  float: left;
  border: 1px ;
  
  width: 100%;
  height: 300px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: #4b0082;
  padding: 17px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 16px;
}
.tab1 button {
  display: block;
  background-color: inherit;
  color: #4b0082;
  padding: 17px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 30px;
}
.tab a {
  display: block;
  background-color: inherit;
  color: #8a2be2;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 16px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: lightgray;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
   /*border: 1px solid #ccc; */ 
  width: 70%;
  border-left: none;
  height: 300px;
}

  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <br>
      <ul class="nav nav-pills nav-stacked">
        <div class="tab1">
          <li><button class="tablinks" onclick="openCity(event, 'developer')" >Developer</button></li>
        </div>
      </ul>
      <br>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">       
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
      <br><br>
      <ul class="nav nav-pills nav-stacked">
         <div class="tab">
      <li><button class="tablinks" onclick="openCity(event, 'Summary')" id="defaultOpen">DashBoard</button></li>
        <li><button class="tablinks" onclick="openCity(event, 'Update Defect')">Update Project Status</button></li>
        <li><button class="tablinks" onclick="openCity(event, 'View Attachments')">View Attachments</button></li>

      <li><button class="tablinks" onclick="openCity(event, 'View Project Details')" >View Project Details</button></li>
      <!-- <li><button class="tablinks" onclick="openCity(event, 'Report Defect')">Report Defect</button></li> -->
      <li><button class="tablinks" onclick="openCity(event, 'View Defect Report')">View Defect Report</button></li>
      <li><a href="login.html">Logout</a></li>

       <br>
       
     </ul>
      <br><br>
      </div>
  
    <div class="col-sm-9">
      <h4><small>Developer Page</small></h4>
      <hr>
      
        <!----------------------------------------------------------VIEW PROJECT DETAILS------------------------------------------------------------->
        <div id="View Project Details" class="tabcontent">
            <h2 style="text-align:left;">View Project Details</h2><br>     
      <div class="container">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Employee Id</th>
        <th>Project Id</th>
        <th>Project Name</th>
        <th>Project Description</th>
        <th>Project Lead</th>
        <th>Submission Date</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
       <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
       <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
       <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
       <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
       <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
       <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
      </tbody>
  </table>
</div>    
      <br><br>
      </div>

<!---------------------------------------------------------------VIEW ATTACHMENTS--------------------------------------------------------->

<div id="View Attachments" class="tabcontent">
<h2 style="text-align:left;">View Attachments</h2><br>     
<div class="container">
<table class="table table-striped">
<thead>
<tr>
<th>Defect Id</th>
<th>Defect Name</th>
<th>Attachments</th>
</tr>
</thead>
<tbody>
<tr>
<td></td><td></td><td></td>
</tr>
<tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr><tr>
<td></td><td></td><td></td>
</tr>
</tbody>
</table>
</div>    
<br><br>
</div>

 <!---------------------------------------------------------VIEW DEFECT REPORT------------------------------------------------------->

 <div id="View Defect Report" class="tabcontent">
      <h2 style="text-align:left;">View Defect Report</h2><br>
      <div class="container"> 
        <table class="table table-striped">
    <thead>
      <tr>
        <th>Defect Id</th>
        <th>Defect Name</th>
        <th>Priority</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
</div>    
      <br><br>
      </div>

<!---------------------------------------------------------------UPDATE DEFECT--------------------------------------------------------->

<div id="Update Defect" class="tabcontent">
      <h2 style="text-align:left;">Update Project Status</h2><br>
      <div class="container">
        <form class="form-horizontal" action="/action_page.php">
          <div class="form-group">
      <label class="control-label col-sm-2" for="projectid">Project Id:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="projectid" placeholder="Enter Project Id" name="projectid">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="projectname">Project Name:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" id="projectname" placeholder="Enter Project Name" name="projectname">
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="projectdesc">Project Description:</label>
      <div class="col-sm-4">
        <textarea class="form-control" rows="5" id="projectdesc" placeholder="Type Project Description" name="projectdesc"></textarea>
      </div>
   </div>
   <div class="form-group">
      <label class="control-label col-sm-2" for="status">Status:</label>
      <div class="col-sm-4">
       <input type="text" class="form-control" id="status" placeholder="Enter Defect Status (InProgress/Completed)" name="status">
      </div>
   </div>
   <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-success">Update Status</button>
      </div>
    </div>

</form>
</div>    
      <br><br>
      </div>
<!-------------------------------------------------------------------SUMMARY------------------------------------------------------------------------->

<div id="Summary" class="tabcontent">
      <h2 style="text-align:left;">Summary of Project</h2><br>
      <div class="container"> 
        <table class="table table-striped">
    <thead>
      <tr>
        <th>Project Id</th>
        <th>Project Name</th>
        <th>Team Lead Name</th>
        <th>Works Done (%)</th>
        <th>Works To Be Done (%)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td><td></td>
      </tr>
     
      
    </tbody>
  </table>
</div>
</div>    
  <!-------------------------------------------------------------TESTER------------------------------------------------------------------->
  <div id="developer" class="tabcontent">
    <h2 style="text-align:left;">Profile</h2><br>
    
    <div class="container">
  
  <form class="form-horizontal" action="/action_page.php">
  <div class="form-group">
    <label class="control-label col-sm-3" for="name">Name:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="managername" placeholder="Name" name="managername">
  </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="emailid">Email ID:</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="emailid" placeholder="Email_ID" name="emailid">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="Changepassword">Change Password:</label>
    <div class="col-sm-4">
      <input type="password" placeholder="*************" class="form-control" required id="changepassword" name="changepassword">
    </div>                                    
  
    <br></br>
  
  <div class="form-group">        
    <div class="col-sm-offset-4 col-sm-2">
      <button type="submit" class="btn btn-success">Update</button>
    </div>
  </div>
  </form>
  </div>    
    </div>
      <br><br>
      </div>
</div>    

<!-----------------------------------------------------------------SCRIPT---------------------------------------------------------------------->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<footer class="container-fluid">
  <p><center>� Cognizant</center></p>
</footer>

</body>
</html>