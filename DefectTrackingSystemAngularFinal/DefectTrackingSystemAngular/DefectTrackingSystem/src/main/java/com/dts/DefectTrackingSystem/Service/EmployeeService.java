package com.dts.DefectTrackingSystem.Service;

import java.util.List;

import com.dts.DefectTrackingSystem.Bean.Employee;

public interface EmployeeService {
	List<Employee> getAllEmployee();
	void saveEmployee(Employee employee);
	Employee getEmployeeById(long id);
	void deleteEmployee(long id);
}
