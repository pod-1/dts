package com.dts.DefectTrackingSystem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dts.DefectTrackingSystem.Bean.Doc;

@Repository
public interface DocRepository extends JpaRepository<Doc, Long>{

}
