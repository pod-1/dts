package com.dts.DefectTrackingSystem.Controller;

import java.util.List;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import com.dts.DefectTrackingSystem.Bean.DoctorAppointment;
import com.dts.DefectTrackingSystem.Service.DocStorageService;
import com.dts.DefectTrackingSystem.Service.DoctorService;
import com.dts.DefectTrackingSystem.Service.ProjectService;
import com.dts.DefectTrackingSystem.Bean.*;

@Controller
public class DoctorAppointmentController {

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private DocStorageService docStorageService;
	// Display List of Doctors
	/*@GetMapping("/")
	public String viewHomePage(Model model) {
		model.addAttribute("listDoctors", doctorService.getAllDoctor());
			
		model.addAttribute("listProject", projectService.getAllProjects());
		
		model.addAttribute("docs", docStorageService.getFiles());
		
		return "doctorAppointment";
	}*/

	@PostMapping("/search")
	public String searchHomePage(Model model, @RequestParam(name = "searchkey") String search) {
		List<DoctorAppointment> lis = doctorService.getAllDoctor();

		List<DoctorAppointment> da = new ArrayList<DoctorAppointment>();

		for (DoctorAppointment d : lis) {

			if (d.getDefectDesc().toLowerCase().contains(search.toLowerCase()) || d.getDefectName().toLowerCase().contains(search.toLowerCase())) {
				da.add(d);
			}
		}

		model.addAttribute("listDoctors", da);
model.addAttribute("listProject", projectService.getAllProjects());
		
		model.addAttribute("docs", docStorageService.getFiles());
		return "doctorAppointment";
	}

	@PostMapping("/filter")
	public String filterHomePage(Model model, @RequestParam(name = "sortby") String sortby) {
		List<DoctorAppointment> lis = doctorService.getAllDoctor();

		Comparator<DoctorAppointment> byName = new Comparator<DoctorAppointment>() {
			@Override
			public int compare(DoctorAppointment o1, DoctorAppointment o2) {
				return o1.getDefectName().compareTo(o2.getDefectName());
			}
		};

		Comparator<DoctorAppointment> byNamed = new Comparator<DoctorAppointment>() {
			@Override
			public int compare(DoctorAppointment o1, DoctorAppointment o2) {
				return o2.getDefectName().compareTo(o1.getDefectName());
			}
		};

		if (sortby.equals("asc"))
			lis.sort(byName);
		else
			lis.sort(byNamed);
		
		model.addAttribute("listDoctors", lis);
		return "redirect:/";
	}
	
	@GetMapping("/showNewDoctorForm")
	public String ShowNewDoctor(Model model) {
		DoctorAppointment d=new DoctorAppointment();
		model.addAttribute("doctor",d);
		return "new_doctor";
	}
	
	@PostMapping("/saveDoctor")
	public String saveDoctor(@ModelAttribute("doctor")DoctorAppointment d) {
		//save Doctor to our Database
		doctorService.saveDoctor(d);
		return "redirect:/";
	}
	
	@GetMapping("/showformUpdate/{id}")
	public String showFormForUpdate(@PathVariable("id") long id,Model model) {
		//get Doctor from the Service
		System.out.println(id);
		DoctorAppointment doctor=doctorService.getEmployeeById(id);
		
		model.addAttribute("doctor", doctor);
		
		return "update_doctor";
	}
	
	@GetMapping("/showformDelete/{id}")
	public String showFormForDelete(@PathVariable("id") long id,Model model) {
		//get Doctor from the Service
		doctorService.deleteDoctor(id);
		
		return "redirect:/";
	}
	
	@RequestMapping("/addProject")
	public String AddProject(@RequestParam("datetime") String datetime,@RequestParam("datetimeend") String datetimeend,Model model) throws ParseException {
		//get Doctor from the Service
		System.out.println(datetime);
		String formatted =datetime.replace("T","");
		String formattedend =datetimeend.replace("T","");
		System.out.println(formatted);
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-ddHH:mm");
		Date dts=format.parse(formatted);
		Date dte=format.parse(formattedend);
		System.out.println(dts);
		Project p=new Project();
		p.setProjectEnd(dte);
		p.setProjectName("DTS");
		p.setProjectStart(dts);
		
		projectService.saveProject(p);
		return "redirect:/";
	}
	
	@GetMapping("/showFileUploadPage")
	public String uploadfilePage() {
		return "fileUpload";
	}
	
	@PostMapping("/uploadFiles")
	public String uploadMultipleFile(@RequestParam("files") MultipartFile file) throws IOException {
		Doc doc=new Doc("System Hang",file.getContentType(),file.getBytes());
		docStorageService.savefile(file, doc);
		
		return "redirect:/";
	}
	
	@GetMapping("/downloadFile/{fileid}")
	public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable("fileid") long fileId){
		Doc doc = docStorageService.getFile(fileId).get();
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(doc.getDocType()))
				.header(HttpHeaders.CONTENT_DISPOSITION,"attachment:filename=\""+doc.getDocName()+"\"")
				.body(new ByteArrayResource(doc.getData()));
	}
	
	@RequestMapping("/Dashboard_profile")
	public String returnDashboard_profile() {
		return "Dashboard_profile";
	}
	
	@RequestMapping("/Dashboard")
	public String returnDashboard() {
		return "redirect:/";
	}
	
	
}
