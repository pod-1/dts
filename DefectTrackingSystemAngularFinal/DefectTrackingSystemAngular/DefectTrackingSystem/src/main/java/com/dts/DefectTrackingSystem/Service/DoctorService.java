package com.dts.DefectTrackingSystem.Service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.dts.DefectTrackingSystem.Bean.Doc;
import com.dts.DefectTrackingSystem.Bean.DoctorAppointment;

public interface DoctorService {
	List<DoctorAppointment> getAllDoctor();
	void saveDoctor(DoctorAppointment doctorAppointment);
	DoctorAppointment getEmployeeById(long id);
	void deleteDoctor(long id);
	void savefile(MultipartFile file, Doc doc);
}
