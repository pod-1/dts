package com.dts.DefectTrackingSystem.Bean;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="doctor")
public class DoctorAppointment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long defectId;
	
	@Column(name="defect_name")
	private String defectName;
	
	@Column(name="defectDesc")
	private String defectDesc;
	
	@Column(name="defect_priority")
	private String defectPriority;
	
	@Column(name="defect_status")
	private String defectStatus;

	@Override
	public String toString() {
		return "DoctorAppointment [defectId=" + defectId + ", defectName=" + defectName + ", defectDesc=" + defectDesc
				+ ", defectPriority=" + defectPriority + ", defectStatus=" + defectStatus + "]";
	}

	public DoctorAppointment() {

	}

	
	
	public DoctorAppointment(String defectName, String defectDesc, String defectPriority, String defectStatus) {
		super();
		this.defectName = defectName;
		this.defectDesc = defectDesc;
		this.defectPriority = defectPriority;
		this.defectStatus = defectStatus;
	}

	public long getDefectId() {
		return defectId;
	}

	public void setDefectId(long defectId) {
		this.defectId = defectId;
	}

	public String getDefectName() {
		return defectName;
	}

	public void setDefectName(String defectName) {
		this.defectName = defectName;
	}

	public String getDefectDesc() {
		return defectDesc;
	}

	public void setDefectDesc(String defectDesc) {
		this.defectDesc = defectDesc;
	}

	public String getDefectPriority() {
		return defectPriority;
	}

	public void setDefectPriority(String defectPriority) {
		this.defectPriority = defectPriority;
	}

	public String getDefectStatus() {
		return defectStatus;
	}

	public void setDefectStatus(String defectStatus) {
		this.defectStatus = defectStatus;
	}

}
