package com.dts.DefectTrackingSystem.Bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "attachments")
public class Attachment {
	
	@Id
	private long id;
	
	private String docDefectName;

	private String docDefectProjecName;

	@Lob
	private byte[] data;

	public String getDocDefectProjecName() {
		return docDefectProjecName;
	}

	public void setDocDefectProjecName(String docDefectProjecName) {
		this.docDefectProjecName = docDefectProjecName;
	}

	public String getDocDefectName() {
		return docDefectName;
	}

	public void setDocDefectName(String docDefectName) {
		this.docDefectName = docDefectName;
	}
}
