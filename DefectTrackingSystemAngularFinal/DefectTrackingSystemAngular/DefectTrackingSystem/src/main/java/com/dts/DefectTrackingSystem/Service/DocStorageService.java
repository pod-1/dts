package com.dts.DefectTrackingSystem.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.dts.DefectTrackingSystem.Bean.Doc;
import com.dts.DefectTrackingSystem.Repository.DocRepository;

@Service
public class DocStorageService implements DocService{
	
	@Autowired
	DocRepository docRepository;
	
	public Doc savefile(MultipartFile file,Doc d) {
		return docRepository.save(d);
		
	}
	
	public Optional<Doc> getFile(long fileId) {
		return docRepository.findById(fileId);
	}
	
	public List<Doc> getFiles(){
		return docRepository.findAll();
	}
}
