package com.dts.DefectTrackingSystem.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dts.DefectTrackingSystem.Bean.Project;
import com.dts.DefectTrackingSystem.Repository.ProjectRepository;

@Service
public class ProjectServiceImpl implements ProjectService{
	
	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public List<Project> getAllProjects() {
		// TODO Auto-generated method stub
		return projectRepository.findAll();
	}

	@Override
	public void saveProject(Project project) {
		// TODO Auto-generated method stub
		this.projectRepository.save(project);
	}

	@Override
	public Project getProjectById(long id) {
		// TODO Auto-generated method stub
		return projectRepository.findById(id).get();
	}

	@Override
	public void deleteProject(long id) {
		// TODO Auto-generated method stub
		
	}

}
