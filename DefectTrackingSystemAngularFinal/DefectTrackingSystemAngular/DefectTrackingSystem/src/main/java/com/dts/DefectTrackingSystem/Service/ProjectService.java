package com.dts.DefectTrackingSystem.Service;

import java.util.List;

import com.dts.DefectTrackingSystem.Bean.Project;

public interface ProjectService {
	List<Project> getAllProjects();
	void saveProject(Project project);
	Project getProjectById(long id);
	void deleteProject(long id);
}
