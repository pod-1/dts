package com.dts.DefectTrackingSystem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dts.DefectTrackingSystem.Bean.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
