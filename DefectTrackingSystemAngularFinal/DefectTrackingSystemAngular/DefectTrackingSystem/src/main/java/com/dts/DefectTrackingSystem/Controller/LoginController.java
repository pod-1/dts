package com.dts.DefectTrackingSystem.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.dts.DefectTrackingSystem.Bean.Defect;
import com.dts.DefectTrackingSystem.Bean.Employee;
import com.dts.DefectTrackingSystem.Service.DefectService;
import com.dts.DefectTrackingSystem.Service.DocStorageService;
import com.dts.DefectTrackingSystem.Service.DoctorService;
import com.dts.DefectTrackingSystem.Service.EmployeeServiceImpl;
import com.dts.DefectTrackingSystem.Service.ProjectService;

@Controller
public class LoginController {
	
	@Autowired
	private EmployeeServiceImpl employeeService;	
	
	@Autowired
	private DoctorService doctorService;

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private DocStorageService docStorageService;
	
	@Autowired
	private DefectService defectService;
	
	@RequestMapping(value="/login")
	public String loginpage() throws ParseException {
	/*	
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		Employee empl=new Employee("Abinash","Acharya","Male",formatter.parse("1998-09-08"),"9898989898","abinash@abc.com","abin123","A123","Developer");
		Employee empl1=new Employee("Imagine","Dragons","Male",formatter.parse("1991-02-09"),"9898989898","imgn@abc.com","imgn123","I123","Tester");
		Employee empl2=new Employee("Freddie","Mercury","Male",formatter.parse("1979-01-04"),"9898989898","freddie@abc.com","fred123","F123","Developer");
		Employee empl3=new Employee("Skye","Black","Female",formatter.parse("1990-10-02"),"9898989898","skye@abc.com","skye123","S123","Manager");
		Employee empl4=new Employee("abc","def","Male",formatter.parse("1999-09-01"),"9898989898","abc@abc.com","abc123","AB123","Tester");
		Employee empl5=new Employee("xxx","yyy","Female",formatter.parse("1988-09-03"),"9898989898","xxx@abc.com","xxx123","X123","Manager");
		employeeService.saveEmployee(empl);
		employeeService.saveEmployee(empl1);
		employeeService.saveEmployee(empl2);
		employeeService.saveEmployee(empl3);
		employeeService.saveEmployee(empl4);
		employeeService.saveEmployee(empl5);
		Defect d1=new Defect("Fix the Bug","Critical",new Date(),7,7,"There is a bug in the system","High","In Progress");
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		Defect def=defectService.getDefectById(1);
		def.setDefectDateRaised(formatter.parse("2021-06-16"));
		def.setDefectEmpIdRaised(2);
		defectService.saveDefect(def);
		*/
		return "LoginViews/login";
	}
	
	@RequestMapping("/logincheck")
	public String loginpage(@RequestParam("username")String username,@RequestParam("password")String password,Model model,HttpSession session) {
		System.out.println(username);
		System.out.println(password);
		List<Employee> listEmployee=employeeService.getAllEmployee();
		Employee xx=null;
		for(Employee e:listEmployee) {
			if(e.getEmail().equals(username))
				xx=e;
		}
		if(xx==null)
		{
			return "LoginViews/Login_error";
		}
		if(xx.getPassword().equals(password))
		{
			session.setAttribute("employee", xx);
			
			if(xx.getRole().equals("Developer")) {
				
				return "redirect:/developer_home";
				
			}
			if(xx.getRole().equals("Tester"))
				return "redirect:/tester_dashboard";
			if(xx.getRole().equals("Manager"))
				return "redirect:/manager_home";
		}
		else {
			model.addAttribute("username",username);
			return "LoginViews/login_error_password";
		}
		return "redirect:/login";
	}
}
