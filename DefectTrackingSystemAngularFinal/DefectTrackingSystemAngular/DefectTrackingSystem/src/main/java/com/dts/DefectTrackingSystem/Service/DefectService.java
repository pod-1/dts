package com.dts.DefectTrackingSystem.Service;

import java.util.List;

import com.dts.DefectTrackingSystem.Bean.Defect;

public interface DefectService {
	List<Defect> getAllDefect();
	void saveDefect(Defect defect);
	Defect getDefectById(long id);
	void deleteDefect(long id);
}
