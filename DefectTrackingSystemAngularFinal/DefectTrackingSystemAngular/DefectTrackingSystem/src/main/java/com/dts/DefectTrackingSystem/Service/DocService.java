package com.dts.DefectTrackingSystem.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.dts.DefectTrackingSystem.Bean.Doc;

public interface DocService {
	
	public Doc savefile(MultipartFile file,Doc d);
	public Optional<Doc> getFile(long fileId);
	public List<Doc> getFiles();
}
