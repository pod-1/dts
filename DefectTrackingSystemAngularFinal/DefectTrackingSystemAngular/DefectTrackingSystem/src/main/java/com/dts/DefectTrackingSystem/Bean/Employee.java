package com.dts.DefectTrackingSystem.Bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long empid;

	private String fName;
	private String lName;
	private String gender;
	private Date dob;
	private String phno;
	private String email;
	private String loginId;
	private String password;
	private String role;

	public Employee() {

	}

	public Employee(String fName, String lName, String gender, Date dob, String phno, String email, String loginId,
			String password, String role) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.gender = gender;
		this.dob = dob;
		this.phno = phno;
		this.email = email;
		this.loginId = loginId;
		this.password = password;
		this.role = role;
	}

	public long getEmpid() {
		return empid;
	}

	public void setEmpid(long empid) {
		this.empid = empid;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPhno() {
		return phno;
	}

	public void setPhno(String phno) {
		this.phno = phno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public Employee(long empid, String fName, String lName, String gender, Date dob, String phno, String email,
			String loginId, String password, String role) {
		super();
		this.empid = empid;
		this.fName = fName;
		this.lName = lName;
		this.gender = gender;
		this.dob = dob;
		this.phno = phno;
		this.email = email;
		this.loginId = loginId;
		this.password = password;
		this.role = role;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", fName=" + fName + ", lName=" + lName + ", gender=" + gender + ", dob="
				+ dob + ", phno=" + phno + ", email=" + email + ", loginId=" + loginId + ", password=" + password
				+ ", role=" + role + "]";
	}
}
