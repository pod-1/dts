package com.dts.DefectTrackingSystem.Controller;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dts.DefectTrackingSystem.Bean.Defect;
import com.dts.DefectTrackingSystem.Bean.Doc;
import com.dts.DefectTrackingSystem.Bean.Employee;
import com.dts.DefectTrackingSystem.Bean.Project;
import com.dts.DefectTrackingSystem.Service.DefectService;
import com.dts.DefectTrackingSystem.Service.DocService;
import com.dts.DefectTrackingSystem.Service.DoctorService;
import com.dts.DefectTrackingSystem.Service.EmployeeService;
import com.dts.DefectTrackingSystem.Service.ProjectService;

@Controller
public class DeveloperViewController {
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private DoctorService doctorService;
	
	@Autowired
	private DefectService defectService;
	
	@Autowired
	private DocService docService;
	
	//Returns the List of Projects That the Employee has
	public List<Project> employeeProjects(Employee emp){
		List<Project> projects=projectService.getAllProjects();
		List<Project> empProjects=new ArrayList<Project>();
		
		
		for(Project p:projects) {
			if(p.getEmpids().contains(emp.getEmpid()+""))
				empProjects.add(p);
		}
		return empProjects;
	}
	@RequestMapping("/developer_home")
	public String developer_dashboard(HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		List<Project> empProjects=employeeProjects(emp);
		session.setAttribute("fName", emp.getfName()+" "+emp.getlName());
		session.setAttribute("role",emp.getRole());
		System.out.println(emp.getRole());
		model.addAttribute("empProjects",empProjects);
		
		
		return "developer/developer_home";
	}

	@RequestMapping("/developer_profile")
	public String developer_profile() {
		return "developer/developer_profile";
	}

	@RequestMapping("/developer_update")
	public String developer_update() {
		
		return "developer/developer_update";
	}

	@RequestMapping("/developer_viewattachment")
	public String developer_viewattachment(HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		List<Doc> doc=docService.getFiles();
		List<Defect> emplDefects=filterDefectsforEmployee(employeeProjects(emp));
		Set<Doc> docsEmpl=new LinkedHashSet<>();
		for(Doc fil:doc) {
			for(Defect d:emplDefects) {
				if(d.getDefectId()==fil.getId())
					docsEmpl.add(fil);
			}
		}
		model.addAttribute("docs",docsEmpl);
		return "developer/developer_viewattachment";
	}
	
	//Filter the Defects According to the Projects Employee has
	public List<Defect> filterDefectsforEmployee(List<Project> empProjects){
		List<Defect> empDefects=new ArrayList<Defect>();
		List<Defect> alldefects=defectService.getAllDefect();
		String s="";
		for(Project p:empProjects) {
			s=s+p.getProjectId()+";";
		}
		System.out.println(s);
		for(Defect d:alldefects) {
			if(s.contains(d.getDefectProjectId()+"") && d.getDefectDateClosed()==null)
				empDefects.add(d);
		}
		return empDefects;
	}
	@RequestMapping("/developer_viewdefectreport")
	public String developer_viewdefectreport(HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		
		List<Project> empProjects=employeeProjects(emp);
		System.out.println(empProjects);
		List<Defect> empDefects=filterDefectsforEmployee(empProjects);
		System.out.println(empDefects);
		model.addAttribute("listDoctors", empDefects);
		
		return "developer/developer_viewdefectreport";
	}
	
	@RequestMapping("/showdefectformUpdateDev/{id}")
	public String showDefectFormForUpdateDev(@PathVariable("id") long id, Model model) {
		//get Doctor from the Service
		System.out.println(id);
		Defect def=defectService.getDefectById(id);
		
		model.addAttribute("doctor", def);
		//doctorService.saveDoctor(doctor);		
		return "tester/tester_dashboard_updatedefect";
	}
	
	@RequestMapping("/developer_viewprojectdetails")
	public String developer_viewprojectdetails(HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");

		List<Project> empProjects=employeeProjects(emp);
			
		model.addAttribute("empProjects",empProjects);
		
		return "developer/developer_viewprojectdetails";
	}
	
	@RequestMapping(value="/update_password_developer",method = RequestMethod.POST)
	public String update_password(@RequestParam("login_id") String login_id,@RequestParam("newpassword")String password,HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		
		if(emp.getLoginId().equals(login_id)) {
			emp.setPassword(password);
			employeeService.saveEmployee(emp);
		}
			
		else {
			model.addAttribute("error_msg","Please Enter A Valid Login Id");
			return "developer/developer_profile";
		}
		return "redirect:/developer_home";
	}
	
	@GetMapping("/downloadFile_developer/{fileid}")
	public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable("fileid") long fileId){
		Doc doc = docService.getFile(fileId).get();
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(doc.getDocType()))
				.header(HttpHeaders.CONTENT_DISPOSITION,"attachment:filename=\""+doc.getDocName()+"\"")
				.body(new ByteArrayResource(doc.getData()));
	}
}