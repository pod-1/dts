package com.dts.DefectTrackingSystem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dts.DefectTrackingSystem.Bean.Defect;

@Repository
public interface DefectRepository extends JpaRepository<Defect, Long>{

}
