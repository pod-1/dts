package com.dts.DefectTrackingSystem.Bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "docs")
public class Doc {
	@Id
	private long id;

	public Doc(long id, String docName, String docType, byte[] data, String docDefectName, String docDefectProjecName) {
		super();
		this.id = id;
		this.docName = docName;
		this.docType = docType;
		this.data = data;
		this.docDefectName = docDefectName;
		this.docDefectProjecName = docDefectProjecName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private String docName;

	private String docType;

	@Lob
	private byte[] data;
	
	private String docDefectName;

	private String docDefectProjecName;


	public Doc() {
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Doc(String docName, String docType, byte[] data) {
		super();
		this.docName = docName;
		this.docType = docType;
		this.data = data;
	}

	public String getDocDefectProjecName() {
		return docDefectProjecName;
	}

	public void setDocDefectProjecName(String docDefectProjecName) {
		this.docDefectProjecName = docDefectProjecName;
	}

	public String getDocDefectName() {
		return docDefectName;
	}

	public void setDocDefectName(String docDefectName) {
		this.docDefectName = docDefectName;
	}

}
