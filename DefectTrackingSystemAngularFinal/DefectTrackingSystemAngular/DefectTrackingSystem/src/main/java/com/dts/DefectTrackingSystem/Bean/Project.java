package com.dts.DefectTrackingSystem.Bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long projectId;

	@Column(name = "project_name")
	private String projectName;

	@Column(name = "project_start")
	private Date projectStart;

	@Column(name = "project_end")
	private Date projectEnd;
	
	@Column(name= "managername")
	private String managername;
	
	public Project() {
		
	}

	public Project(String projectName, Date projectStart, Date projectEnd, String managername,
			String empids) {
		super();
		this.projectName = projectName;
		this.projectStart = projectStart;
		this.projectEnd = projectEnd;
		this.managername = managername;
		this.empids = empids;
	}

	public String getmanagername() {
		return managername;
	}

	public void setmanagername(String managername) {
		this.managername = managername;
	}

	public Project(long projectId, String projectName, Date projectStart, Date projectEnd, String managername,
			String empids) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectStart = projectStart;
		this.projectEnd = projectEnd;
		this.managername = managername;
		this.empids = empids;
	}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", projectName=" + projectName + ", projectStart=" + projectStart
				+ ", projectEnd=" + projectEnd + ", managername=" + managername + ", empids=" + empids + "]";
	}

	private String empids;

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getProjectStart() {
		return projectStart;
	}

	public void setProjectStart(Date projectStart) {
		this.projectStart = projectStart;
	}

	public Date getProjectEnd() {
		return projectEnd;
	}

	public void setProjectEnd(Date projectEnd) {
		this.projectEnd = projectEnd;
	}

	public String getEmpids() {
		return empids;
	}

	public void setEmpids(String empids) {
		this.empids = empids;
	}
}
