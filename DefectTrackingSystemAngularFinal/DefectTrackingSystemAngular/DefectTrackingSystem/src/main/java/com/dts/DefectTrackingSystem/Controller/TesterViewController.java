package com.dts.DefectTrackingSystem.Controller;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.dts.DefectTrackingSystem.Bean.Defect;
import com.dts.DefectTrackingSystem.Bean.Doc;
import com.dts.DefectTrackingSystem.Bean.DoctorAppointment;
import com.dts.DefectTrackingSystem.Bean.Employee;
import com.dts.DefectTrackingSystem.Bean.Project;
import com.dts.DefectTrackingSystem.Service.DefectService;
import com.dts.DefectTrackingSystem.Service.DocService;
import com.dts.DefectTrackingSystem.Service.DoctorService;
import com.dts.DefectTrackingSystem.Service.EmployeeService;
import com.dts.DefectTrackingSystem.Service.ProjectService;


//This Contoller is only to transfer the view between the Tester Dashboard pages

@Controller
public class TesterViewController {
	
	@Autowired
	private DoctorService doctorService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired 
	private DefectService defectService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private DocService docService;
	
	//Returns the List of Projects That the Employee has
		public List<Project> employeeProjects(Employee emp){
			List<Project> projects=projectService.getAllProjects();
			List<Project> empProjects=new ArrayList<Project>();
			
			
			for(Project p:projects) {
				if(p.getEmpids().contains(emp.getEmpid()+""))
					empProjects.add(p);
			}
			return empProjects;
		}

	
	@RequestMapping("/tester_dashboard")
	public String tester_dashboard(HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		List<Project> projects=projectService.getAllProjects();
		List<Project> empProjects=new ArrayList<Project>();
		
		session.setAttribute("fName", emp.getfName()+" "+emp.getlName());
		session.setAttribute("role",emp.getRole());
		System.out.println(emp);
		
		for(Project p:projects) {
			if(p.getEmpids().contains(emp.getEmpid()+""))
				empProjects.add(p);
		}
		model.addAttribute("empProjects",empProjects);
		return "tester/tester_dashboard";
	}
	
	//Filter the Defects According to the Projects Employee has
	public List<Defect> filterDefectsforTester(Employee employee){
		List<Defect> empDefects=new ArrayList<Defect>();
		List<Defect> alldefects=defectService.getAllDefect();
		
		for(Defect d:alldefects) {
			if(d.getDefectEmpIdRaised()== employee.getEmpid() && d.getDefectDateClosed()==null)
				empDefects.add(d);
		}
		return empDefects;
	}
	
	@RequestMapping("/tester_dashboard_index")
	public String tester_dashboard_index(HttpSession session, Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		List<Project> projects=projectService.getAllProjects();
		List<Project> empProjects=new ArrayList<Project>();
		
		System.out.println(emp);
		
		for(Project p:projects) {
			if(p.getEmpids().contains(emp.getEmpid()+""))
				empProjects.add(p);
		}
		model.addAttribute("empProjects",empProjects);
		return "tester/tester_dashboard_index";
	}
	
	@RequestMapping("/tester_dashboard_report")
	public String tester_dashboard_report() {
		return "tester/tester_dashboard_report";
	}
	
	@RequestMapping("/tester_dashboard_viewdefectreport")
	public String tester_dashboard_viewdefectreport(HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		List<Defect> defectsRaised=filterDefectsforTester(emp);
		model.addAttribute("listDoctors",defectsRaised);
		
		return "tester/tester_dashboard_viewdefectreport";
	}
	
	@RequestMapping("/tester_dashboard_updatedefect")
	public String tester_dashboard_updatedefect() {
		return "tester/tester_dashboard_updatedefect";
	}
	
	@RequestMapping("/tester_dashboard_profile")
	public String tester_dashboard_profile() {
		return "tester/tester_dashboard_profile";
	}
	
	@RequestMapping("/showdefectformUpdate/{id}")
	public String showDefectFormForUpdate(@PathVariable("id") long id, Model model) {
		//get Doctor from the Service
		System.out.println(id);
		Defect def=defectService.getDefectById(id);
		
		model.addAttribute("doctor", def);
		//doctorService.saveDoctor(doctor);		
		return "tester/tester_dashboard_updatedefect";
	}
	
	@RequestMapping("/showdefectformDelete/{id}")
	public String showDefectFormForDelete(@PathVariable("id") long id,Model model) {
		//get Doctor from the Service
		doctorService.deleteDoctor(id);
		
		return "redirect:/developer_viewdefectreport";
	}
	
	@RequestMapping(value="/saveDefect", method = RequestMethod.POST)
	public String saveDefect(@ModelAttribute("doctor")Defect d) {
		//save Doctor to our Database
		Defect def=defectService.getDefectById(d.getDefectId());
		def.setDefectPriority(d.getDefectPriority());
		def.setDefectStatus(d.getDefectStatus());
		defectService.saveDefect(def);
		return "redirect:/developer_viewdefectreport";
	}
	
	
	@RequestMapping(value="/ReportDefect", method = RequestMethod.POST)
	public String Add_Report(@RequestParam("defectname")String defectName,@RequestParam("defecttype") String defectType,@RequestParam("ddesc")String defectDesc,@RequestParam("priority")String defectPriority,@RequestParam("status")String defectStatus,HttpSession session,Model model) throws IOException 
	{
		String pid=(String)session.getAttribute("projectId");
		System.out.println(pid);
		Long projectId=Long.parseLong(pid);
		Employee emp=(Employee)session.getAttribute("employee");
		Defect defect=new Defect(defectName, defectType,new Date(),projectId, emp.getEmpid(), defectDesc, defectPriority, defectStatus);
		defectService.saveDefect(defect);
		return "redirect:/tester_dashboard";
	}
	
	@RequestMapping(value="/reportDefect")
	public String report_Defect(@RequestParam("id") String pid,HttpSession session,Model model) {
		session.setAttribute("projectId", pid);
		return "tester/tester_dashboard_report";
	}
	
	@RequestMapping(value="/update_password_tester",method = RequestMethod.POST)
	public String update_password(@RequestParam("login_id") String login_id,@RequestParam("newpassword")String password,HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		
		if(emp.getLoginId().equals(login_id)) {
			emp.setPassword(password);
			employeeService.saveEmployee(emp);
		}
			
		else {
			model.addAttribute("error_msg","Please Enter A Valid Login Id");
			return "tester/tester_dashboard_profile";
		}
		return "redirect:/tester_dashboard";
	}
	
	@GetMapping("/showFileUploadPage_Tester")
	public String uploadfilePage(@RequestParam("id") String id,HttpSession session,Model model) {
		Defect defect=defectService.getDefectById(Long.parseLong(id));
		session.setAttribute("defect", defect);
		return "tester/tester_dashboard_fileupload";
	}
	
	@RequestMapping(value="/uploadFiles_Tester",method = RequestMethod.POST)
	public String uploadMultipleFile(@RequestParam("files") MultipartFile file,HttpSession session) throws IOException {
		Defect defect=(Defect)session.getAttribute("defect"); 
		Project project=projectService.getProjectById(defect.getDefectProjectId());
		Doc doc=new Doc(defect.getDefectId(),defect.getDefectName(),file.getContentType(),file.getBytes(), defect.getDefectName(),project.getProjectName());
		docService.savefile(file, doc);
		
		return "redirect:/tester_dashboard_viewdefectreport";
	}
	
	@RequestMapping(value="/closeDefect_Tester")
	public String closeDefect(@RequestParam("id")String id) {
		Defect defect=defectService.getDefectById(Long.parseLong(id));
		defect.setDefectDateClosed(new Date());
		defectService.saveDefect(defect);
		return "redirect:/tester_dashboard_viewdefectreport";
	}
	
}

