package com.dts.DefectTrackingSystem.Controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dts.DefectTrackingSystem.Bean.Employee;
import com.dts.DefectTrackingSystem.Service.EmployeeService;

@Controller
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value="/show")
	public String showEmployee(Model model) {
		model.addAttribute("listEmployees", employeeService.getAllEmployee());
		return "index";
	}
	
	@RequestMapping(value="/showLogin")
	public String showLogin(Model model) {
		model.addAttribute("listEmployees", employeeService.getAllEmployee());
		return "/LoginViews/login";
	}
	
}
