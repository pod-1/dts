package com.dts.DefectTrackingSystem.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dts.DefectTrackingSystem.Bean.Defect;
import com.dts.DefectTrackingSystem.Bean.Employee;
import com.dts.DefectTrackingSystem.Bean.Project;
import com.dts.DefectTrackingSystem.Service.DefectService;
import com.dts.DefectTrackingSystem.Service.DoctorService;
import com.dts.DefectTrackingSystem.Service.EmployeeService;
import com.dts.DefectTrackingSystem.Service.EmployeeServiceImpl;
import com.dts.DefectTrackingSystem.Service.ProjectService;

@Controller
public class ManagerViewController {

	@Autowired
	private DoctorService doctorService;
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeServiceImpl employeeService;
	
	@Autowired
	private DefectService defectService;
	
	//Method to Extract projects of a Particular Employee
	public List<Project> getProjectsOfEmployee(Employee emp) {
		List<Project> empProjects = new ArrayList<Project>();
		List<Project> projects =projectService.getAllProjects();
		
		
		for (Project p : projects) {
			if (p.getEmpids().contains(emp.getEmpid() + ""))
				empProjects.add(p); 
		}
		return empProjects;
	}

	@RequestMapping("/manager_home")
	public String manager_dashboard(HttpSession session, Model model) {

		Employee emp = (Employee) session.getAttribute("employee");
		List<Project> empProjects = getProjectsOfEmployee(emp);
		
		session.setAttribute("fName", emp.getfName()+" "+emp.getlName());
		session.setAttribute("role",emp.getRole());

		model.addAttribute("empProjects", empProjects);

		return "manager/manager_home";
	}

	@RequestMapping("/manager_profile")
	public String manager_profile() {
		return "manager/manager_profile";
	}

	@RequestMapping("/manager_addnewuser")
	public String manager_addnewuser() {
		return "manager/manager_addnewuser";
	}

	@RequestMapping("/manager_createproject")
	public String showmanager_createproject(Model model) {
		model.addAttribute("msg", "Valid_Employee");
		return "manager/manager_createproject";
	}
	
	//Converts String to Date Datatype
	public Date stringToDateConverter(String dt) throws ParseException {
		String formatted = dt.replace("T", "");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dts = format.parse(formatted);
		return dts;
	}

	@RequestMapping(value = "/manager_createproject_Save", method = RequestMethod.POST)
	public String manager_createproject(@RequestParam("pname") String pname,
			@RequestParam("projectStart") String pstart, @RequestParam("projectEnd") String pend,
			@RequestParam("leadid") String pmanagerid, Model model) throws ParseException {
		List<Employee> listemp = employeeService.getAllEmployee();
		Employee projectmanager = null;
		for (Employee e : listemp) {
			if ((e.getEmpid() == Long.parseLong(pmanagerid)) && (e.getRole().equals("Manager")))
				projectmanager = e;
		}
		if (projectmanager == null) {

			return "manager/manager_createproject_error";
		}
		Date dts = stringToDateConverter(pstart);
		Date dte = stringToDateConverter(pend);
		String manager_name = projectmanager.getfName() + " " + projectmanager.getlName();
		String empids = "" + projectmanager.getEmpid();
		Project project = new Project(pname, dts, dte, manager_name, empids);
		projectService.saveProject(project);
		return "redirect:/manager_home";
	}

	@RequestMapping("/manager_projectmembers")
	public String manager_projectmembers(HttpSession session, Model model) {
		Employee emp = (Employee) session.getAttribute("employee");
		List<Project> projects = projectService.getAllProjects();
		List<Project> empProjects = new ArrayList<Project>();
		List<String> projectforempl = new ArrayList<String>();
		for (Project p : projects) {
			if (p.getEmpids().contains(emp.getEmpid() + ""))
				empProjects.add(p);
		}
		Set<Employee> lis = new LinkedHashSet<Employee>();

		for (Project p : empProjects) {
			String arr[] = p.getEmpids().split(";");
			for (String s : arr) {
				if (emp.getEmpid() == Long.parseLong(s))
					continue;
				lis.add(employeeService.getEmployeeById(Long.parseLong(s)));

			}
		}
		System.out.println(lis);

		model.addAttribute("projectMembers", lis);

		return "manager/manager_projectmembers";
	}

	@RequestMapping("/manager_updateuserdetails")
	public String manager_updateuserdetails() {
		return "manager/manager_updateuserdetails";
	}
	
	//Filter the Defects According to the Projects Employee has
		public List<Defect> filterDefectsforEmployee(List<Project> empProjects){
			List<Defect> empDefects=new ArrayList<Defect>();
			List<Defect> alldefects=defectService.getAllDefect();
			String s="";
			for(Project p:empProjects) {
				s=s+p.getProjectId()+";";
			}
			System.out.println(s);
			for(Defect d:alldefects) {
				if(s.contains(d.getDefectProjectId()+"") && d.getDefectDateClosed()==null)
					empDefects.add(d);
			}
			return empDefects;
		}

	@RequestMapping("/manager_viewdefectreport")
	public String manager_viewdefectreport(HttpSession session,Model model) {
		
		Employee emp=(Employee)session.getAttribute("employee");
		
		List<Project> empProjects=getProjectsOfEmployee(emp);
		System.out.println(empProjects);
		List<Defect> empDefects=filterDefectsforEmployee(empProjects);
		System.out.println(empDefects);
		model.addAttribute("listDoctors", empDefects);
		return "manager/manager_viewdefectreport";
	}

	public List<Employee> employeesPartofProject(Project proj){
		List<Employee> employees = new ArrayList<Employee>();

		String arr[] = proj.getEmpids().split(";");
		for (String s : arr) {
			employees.add(employeeService.getEmployeeById(Long.parseLong(s)));
		}
		return employees;
	}
	@RequestMapping("/showProjectMembers")
	public String manager_updateuserdetails(@RequestParam("id") long id, Model model, HttpSession session) {

		Employee emp = (Employee) session.getAttribute("employee");
		Project project = projectService.getProjectById(id);
		List<Employee> employees = employeesPartofProject(project);

		System.out.println(employees);

		model.addAttribute("projectMembers", employees);

		return "manager/manager_projectmembers";

	}
	
	//Function to generate Random Password
	public String randomPassword(int len) {

		// Using numeric values
		String numbers = "0123456789";

		// Using random method
		Random rndm_method = new Random();

		String x = "";

		for (int i = 0; i < len; i++) {
			x = x + numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return x;
	}

	@RequestMapping(value = "/AddNewUser", method = RequestMethod.POST)
	public String AddNewUser(@RequestParam("fname") String fName, @RequestParam("lname") String lName,
			@RequestParam("role") String role, @RequestParam("eid") String emailid, @RequestParam("dob") String dob,
			@RequestParam("gender") String gender, @RequestParam("lid") String loginid,
			@RequestParam("cno") String phno, Model model) throws ParseException {

		Date dateob = stringToDateConverter(dob);

		boolean notuniqueid = false;
		List<Employee> emplist = employeeService.getAllEmployee();
		for (Employee e : emplist) {
			if (e.getEmail().equals(emailid)) {
				notuniqueid = true;
				break;
			}
		}
		if (notuniqueid) {
			model.addAttribute("msg", "Please Enter a Unique Email Id");
			model.addAttribute("fname", fName);
			model.addAttribute("lname", lName);
			model.addAttribute("dob", dob);

			model.addAttribute("eid", emailid);
			model.addAttribute("lid", loginid);
			model.addAttribute("cno", phno);
			return "manager/manager_addnewuser_error";
		}
		String password = randomPassword(5);
		System.out.println(password);
		Employee employee = new Employee(fName, lName, gender, dateob, phno, emailid, loginid, password, role);
		employeeService.saveEmployee(employee);
		return "redirect:/manager_home";

	}
	
	//Returns The List of Employees That are not the part of Project
	public List<Employee> employeesNotInProject(String pid){
		List<Employee> employees = new ArrayList<Employee>();
		List<Employee> allemployees = employeeService.getAllEmployee();
		Project project = projectService.getProjectById(Long.parseLong(pid));
		for (Employee e : allemployees) {
			if (!project.getEmpids().contains(e.getEmpid() + "") && !e.getRole().equals("Manager"))
				employees.add(e);
		}
		return employees;
	}

	@RequestMapping("/addProjectMembers")
	public String addMembers(@RequestParam("id") String pid, Model model, HttpSession session) {
		List<Employee> employees = employeesNotInProject(pid);
		List<Employee> allemployees = employeeService.getAllEmployee();
		Project project = projectService.getProjectById(Long.parseLong(pid));
		
		session.setAttribute("project", project);
		session.setAttribute("projectname", project.getProjectName());
		model.addAttribute("freeemployees", employees);
		return "manager/manager_addprojectmembers";
	}
	//Filter Projects by search key
	public List<Project> filterBySearchkeyProject(Employee emp,String searchkey){
		List<Project> projects = projectService.getAllProjects();
		List<Project> empProjects = getProjectsOfEmployee(emp);
		List<Project> filterProjects = new ArrayList<Project>();
		for (Project p : empProjects) {
			String pid = p.getProjectId() + "";
			if (pid.contains(searchkey) || p.getProjectName().contains(searchkey)
					|| p.getmanagername().contains(searchkey))
				filterProjects.add(p);
		}
		return filterProjects;
	}
	
	@RequestMapping("/search_manager")
	public String searchProject(@RequestParam("searchkey") String searchkey, HttpSession session, Model model) {
		Employee emp = (Employee) session.getAttribute("employee");
		List<Project> projects = projectService.getAllProjects();
		List<Project> empProjects = getProjectsOfEmployee(emp);
		List<Project> filterProjects = new ArrayList<Project>();
		System.out.println(emp);



		if (searchkey.equals(""))
			model.addAttribute("empProjects", empProjects);
		else {
			filterProjects=filterBySearchkeyProject(emp,searchkey);
		}

		if (filterProjects.isEmpty())
			model.addAttribute("empProjects", empProjects);
		else {
			model.addAttribute("empProjects", filterProjects);
		}
		return "manager/manager_home";
	}

	@RequestMapping("/addMember")
	public String addMember(@RequestParam("id") String empid, HttpSession session) {
		Project project = (Project) session.getAttribute("project");
		String pid = project.getProjectId() + "";
		String link = "redirect:/addProjectMembers?id=" + pid;
		project.setEmpids(project.getEmpids() + ";" + empid);
		projectService.saveProject(project);
		return link;
	}
	
	@RequestMapping(value="/update_password_manager",method = RequestMethod.POST)
	public String update_password(@RequestParam("login_id") String login_id,@RequestParam("newpassword")String password,HttpSession session,Model model) {
		Employee emp=(Employee)session.getAttribute("employee");
		
		if(emp.getLoginId().equals(login_id)) {
			emp.setPassword(password);
			employeeService.saveEmployee(emp);
		}
			
		else {
			model.addAttribute("error_msg","Please Enter A Valid Login Id");
			return "manager/manager_profile";
		}
		return "redirect:/manager_home";
	}
}
	
	


