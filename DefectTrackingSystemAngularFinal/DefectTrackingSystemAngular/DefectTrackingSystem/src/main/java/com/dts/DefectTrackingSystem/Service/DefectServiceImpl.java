package com.dts.DefectTrackingSystem.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dts.DefectTrackingSystem.Bean.Defect;
import com.dts.DefectTrackingSystem.Repository.DefectRepository;

@Service
public class DefectServiceImpl implements DefectService{
	@Autowired
	private DefectRepository defectRepository;
	
	@Override
	public List<Defect> getAllDefect() {
		// TODO Auto-generated method stub
		return defectRepository.findAll();
	}

	@Override
	public void saveDefect(Defect defect) {
		// TODO Auto-generated method stub
		this.defectRepository.save(defect);
	}

	@Override
	public Defect getDefectById(long id) {
		// TODO Auto-generated method stub
		Optional<Defect> findById = defectRepository.findById(id);
		return findById.get();
	}

	@Override
	public void deleteDefect(long id) {
		// TODO Auto-generated method stub
		defectRepository.deleteById(id);
	}
}
