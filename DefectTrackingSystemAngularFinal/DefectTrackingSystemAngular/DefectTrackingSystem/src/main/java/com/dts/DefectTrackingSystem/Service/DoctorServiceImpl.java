package com.dts.DefectTrackingSystem.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.dts.DefectTrackingSystem.Bean.Doc;
import com.dts.DefectTrackingSystem.Bean.DoctorAppointment;
import com.dts.DefectTrackingSystem.Repository.DoctorRepository;

@Service
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	private DoctorRepository doctorRepository;

	@Override
	public List<DoctorAppointment> getAllDoctor() {
		// TODO Auto-generated method stub
		return doctorRepository.findAll();
	}

	@Override
	public void saveDoctor(DoctorAppointment doctorAppointment) {
		// TODO Auto-generated method stub
		this.doctorRepository.save(doctorAppointment);
	}

	@Override
	public DoctorAppointment getEmployeeById(long id) {
		// TODO Auto-generated method stub
		Optional<DoctorAppointment> findById = doctorRepository.findById(id);
		DoctorAppointment doctor=null;
		if(findById.isPresent()) {
			doctor=findById.get();
		}else
		{
			throw  new RuntimeException(" Employee not found for id :: "+id);
		}
		
		return doctor;
	}

	@Override
	public void deleteDoctor(long id) {
		// TODO Auto-generated method stub
		
		doctorRepository.deleteById(id);
		
	}

	@Override
	public void savefile(MultipartFile file, Doc doc) {
		// TODO Auto-generated method stub
		
	}

}
