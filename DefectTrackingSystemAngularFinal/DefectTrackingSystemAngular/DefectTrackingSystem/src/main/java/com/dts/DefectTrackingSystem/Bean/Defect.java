package com.dts.DefectTrackingSystem.Bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "defect")
public class Defect {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long defectId;

	@Column(name = "defect_name")
	private String defectName;

	@Column(name = "defect_type")
	private String defectType;

	@Column(name = "defect_dateraised")
	private Date defectDateRaised;

	@Column(name = "defect_dateclosed")
	private Date defectDateClosed;

	@Column(name = "defect_projectid")
	private long defectProjectId;

	@Column(name = "defect_empidraised")
	private long defectEmpIdRaised;

	@Column(name = "defectDesc")
	private String defectDesc;

	@Column(name = "defect_priority")
	private String defectPriority;

	@Column(name = "defect_status")
	private String defectStatus;

	public String getDefectType() {
		return defectType;
	}

	public void setDefectType(String defectType) {
		this.defectType = defectType;
	}

	public Date getDefectDateRaised() {
		return defectDateRaised;
	}

	public void setDefectDateRaised(Date defectDateRaised) {
		this.defectDateRaised = defectDateRaised;
	}

	public Date getDefectDateClosed() {
		return defectDateClosed;
	}

	public void setDefectDateClosed(Date defectDateClosed) {
		this.defectDateClosed = defectDateClosed;
	}

	public long getDefectProjectId() {
		return defectProjectId;
	}

	public void setDefectProjectId(long defectProjectId) {
		this.defectProjectId = defectProjectId;
	}

	public long getDefectEmpIdRaised() {
		return defectEmpIdRaised;
	}

	public void setDefectEmpIdRaised(long defectEmpIdRaised) {
		this.defectEmpIdRaised = defectEmpIdRaised;
	}

	public Defect(String defectName, String defectType, Date defectDateRaised, 
			long defectProjectId, long defectEmpIdRaised, String defectDesc, String defectPriority,
			String defectStatus) {
		super();
		this.defectName = defectName;
		this.defectType = defectType;
		this.defectDateRaised = defectDateRaised;
		this.defectProjectId = defectProjectId;
		this.defectEmpIdRaised = defectEmpIdRaised;
		this.defectDesc = defectDesc;
		this.defectPriority = defectPriority;
		this.defectStatus = defectStatus;
	}

	@Override
	public String toString() {
		return "Defect [defectId=" + defectId + ", defectName=" + defectName + ", defectType=" + defectType
				+ ", defectDateRaised=" + defectDateRaised + ", defectDateClosed=" + defectDateClosed
				+ ", defectProjectId=" + defectProjectId + ", defectEmpIdRaised=" + defectEmpIdRaised + ", defectDesc="
				+ defectDesc + ", defectPriority=" + defectPriority + ", defectStatus=" + defectStatus + "]";
	}

	public Defect() {

	}

	public long getDefectId() {
		return defectId;
	}

	public void setDefectId(long defectId) {
		this.defectId = defectId;
	}

	public String getDefectName() {
		return defectName;
	}

	public void setDefectName(String defectName) {
		this.defectName = defectName;
	}

	public String getDefectDesc() {
		return defectDesc;
	}

	public void setDefectDesc(String defectDesc) {
		this.defectDesc = defectDesc;
	}

	public String getDefectPriority() {
		return defectPriority;
	}

	public Defect(long defectId, String defectName, String defectType, Date defectDateRaised, Date defectDateClosed,
			long defectProjectId, long defectEmpIdRaised, String defectDesc, String defectPriority,
			String defectStatus) {
		super();
		this.defectId = defectId;
		this.defectName = defectName;
		this.defectType = defectType;
		this.defectDateRaised = defectDateRaised;
		this.defectDateClosed = defectDateClosed;
		this.defectProjectId = defectProjectId;
		this.defectEmpIdRaised = defectEmpIdRaised;
		this.defectDesc = defectDesc;
		this.defectPriority = defectPriority;
		this.defectStatus = defectStatus;
	}

	public void setDefectPriority(String defectPriority) {
		this.defectPriority = defectPriority;
	}

	public String getDefectStatus() {
		return defectStatus;
	}

	public void setDefectStatus(String defectStatus) {
		this.defectStatus = defectStatus;
	}
}