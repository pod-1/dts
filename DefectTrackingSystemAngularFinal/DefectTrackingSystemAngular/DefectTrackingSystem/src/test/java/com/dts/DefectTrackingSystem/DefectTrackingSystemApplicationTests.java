package com.dts.DefectTrackingSystem;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.dts.DefectTrackingSystem.Bean.Defect;
import com.dts.DefectTrackingSystem.Bean.Employee;
import com.dts.DefectTrackingSystem.Bean.Project;
import com.dts.DefectTrackingSystem.Controller.ManagerViewController;
import com.dts.DefectTrackingSystem.Repository.DefectRepository;
import com.dts.DefectTrackingSystem.Repository.EmployeeRepository;
import com.dts.DefectTrackingSystem.Repository.ProjectRepository;
import com.dts.DefectTrackingSystem.Service.DefectService;
import com.dts.DefectTrackingSystem.Service.EmployeeService;
import com.dts.DefectTrackingSystem.Service.ProjectService;

@SpringBootTest
class DefectTrackingSystemApplicationTests {
	
	ManagerViewController mvc = new ManagerViewController();
	
	@Autowired
	private DefectService defectService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ProjectService projectService;
	
	@MockBean private DefectRepository defectrepo;
	@MockBean private EmployeeRepository employeerepo;
	@MockBean private ProjectRepository projectrepo;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void getAllEmployeesTest() throws ParseException
	{
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		when(employeerepo.findAll()).thenReturn(Stream.of(new Employee("Abinash","Acharya","Male",formatter.parse("1998-09-08"),"9898989898","abinash@abc.com","abin123","A123","Developer"),
				new Employee("Imagine","Dragons","Male",formatter.parse("1991-02-09"),"9898989898","imgn@abc.com","imgn123","I123","Tester")).collect(Collectors.toList()));
		
		assertEquals(2,employeerepo.findAll().size());	
	}
	
	@Test
	public void findEmployeesByIdTest() throws ParseException
	{
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		when(employeerepo.findById((long) 2)).thenReturn(Optional.of(new Employee(2,"Imagine","Dragons","Male",formatter.parse("1991-02-09"),"9898989898","imgn@abc.com","imgn123","I123","Tester")));
		
		assertEquals(2,employeerepo.findById((long) 2).get().getEmpid());	
	}
	
	@Test
	public void saveEmployeeTest() throws ParseException
	{
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		Employee employee=new Employee(2,"Imagine","Dragons","Male",formatter.parse("1991-02-09"),"9898989898","imgn@abc.com","imgn123","I123","Tester");
		when(employeerepo.save(employee)).thenReturn(new Employee(2,"Imagine","Dragons","Male",formatter.parse("1991-02-09"),"9898989898","imgn@abc.com","imgn123","I123","Tester"));
		
		assertTrue(employee.getEmail().equals(employeerepo.save(employee).getEmail()));	
	}
	
	@Test
	public void findAllDefectsTest() 
	{
		//new Defect(1,"Fix the Bug","Critical",new Date(),new Date(),7,7,"There is a bug in the system","High","In Progress")
		when(defectrepo.findAll()).thenReturn(Stream.of(new Defect(1,"Fix the Bug","Critical",new Date(),new Date(),7,7,"There is a bug in the system","High","In Progress"),
				new Defect(2,"Another Bug","Less Critical",new Date(),new Date(),7,7,"There is a bug in the system","High","In Progress")).collect(Collectors.toList()));
		
		assertEquals(2,defectrepo.findAll().size());	
	}
	
	@Test
	public void findDefectsByIdTest() 
	{
		when(defectrepo.findById((long) 1)).thenReturn(Optional.of(new Defect(1,"Fix the Bug","Critical",new Date(),new Date(),7,7,"There is a bug in the system","High","In Progress")));
		
		assertEquals(1,defectrepo.findById((long) 1).get().getDefectId());	
	}
	
	@Test
	public void saveDefectTest() 
	{
		Defect defect=new Defect(1,"Fix the Bug","Critical",new Date(),new Date(),7,7,"There is a bug in the system","High","In Progress");
		when(defectrepo.save(defect)).thenReturn(new Defect(1,"Fix the Bug","Critical",new Date(),new Date(),7,7,"There is a bug in the system","High","In Progress"));
		System.out.println(defect);
		System.out.println(defectrepo.save(defect).getDefectName());
		assertTrue(defect.getDefectId()==defectrepo.save(defect).getDefectId());	
	}
	
	@Test
	public void getAllProjectsTest() 
	{
		when(projectrepo.findAll()).thenReturn(Stream.of(new Project(1,"DTS",new Date(),new Date(),"Skye Black","1;2;3;4;7;9"),
				new Project(2,"DTS",new Date(),new Date(),"Skye Black","1;2;3;4;7;9")).collect(Collectors.toList()));
		
		assertEquals(2,projectrepo.findAll().size());	
	}
	
	@Test
	public void findProjectByIdTest() 
	{
		when(projectrepo.findById((long) 3)).thenReturn(Optional.of(new Project(3,"DTS",new Date(),new Date(),"Skye Black","1;2;3;4;7;9")));
		
		assertEquals(3,projectrepo.findById((long) 3).get().getProjectId());	
	}
	
	@Test
	public void saveProjectTest() 
	{
		Project project=new Project(3,"DTS",new Date(),new Date(),"Skye Black","1;2;3;4;7;9");
		when(projectrepo.save(project)).thenReturn(new Project(3,"DTS",new Date(),new Date(),"Skye Black","1;2;3;4;7;9"));
		assertTrue(project.getProjectId()==projectrepo.save(project).getProjectId());	
	}
	
	

	@Test
	void testStringToDateConverter() throws ParseException {
		Date date1 = new Date(98, 4, 3);
		System.out.println(date1);
		Date date = mvc.stringToDateConverter("1998-05-03");
		System.out.println(date);
		assertTrue(date1.equals(date));
	}
	
	@Test 
	void testRandomPassword() {
		String pass=mvc.randomPassword(5);
		boolean valid=true;
		for(int i=0;i<pass.length();i++) {
			if(pass.charAt(i)<'0' && pass.charAt(i)>'9')
				valid=false;
		}
		assertTrue(valid);
	}


}
